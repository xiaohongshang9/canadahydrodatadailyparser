###This is a tutorial regarding uploading Canadian Hydrometric Data
###Step-by-step guide
####Get the required dependency
    install node environment, then execute the following in command line
	npm install csv
	npm install path
	npm install async
	npm install request
	npm install csv-stream
	npm install node-schedule
####Create Things (in this example code, it creates AB stations)
	node create_things.js
	node create_datastream.js
####Retrieve daily observation data (from http://dd.weather.gc.ca/hydrometric/csv/)and parse. Code will be run automatically at 8:30am everyday to retrieve data of previous day and upload to sensorThings service
	node csv_daily_data-parser.js