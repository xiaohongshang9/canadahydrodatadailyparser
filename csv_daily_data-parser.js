var http = require('http');
var fs = require('fs');
var request = require('request');
var csv = require('csv-stream');
var later = require('later');
var cron = require('node-schedule');
var obserURL = "http://184.70.194.230/OGCSensorThings/v1.0/Observations";
var canada_stations=['AB','BC','MB','NB','NL','NS','NT','NS','NT','NU','ON','PE','QC','SK','YT'];
//var reqURL = "http://dd.weather.gc.ca/hydrometric/csv/AB/daily/AB_daily_hydrometric.csv";
var streamID_stationID = "datastreamID_stationID_Dict.json";
var headers = {
	'Content-Type': 'application/json'
};
var runtimes = 0;
var totalData = '';
var yestoday;
var rule = new cron.RecurrenceRule();
rule.dayOfWeek = [1, 2, 3, 4, 5, 6, 0];
rule.hour = 8;
rule.minute = 30;
cron.scheduleJob(rule, init);

function init() {
	console.log('start running,this is the' + runtimes + "run this programm")
	runtimes++;
	fs.readFile(streamID_stationID, 'utf8', function(err, IDdata) {
		var IDfile;
		IDfile = JSON.parse(IDdata);
		csvParser(IDfile);
	});
};

function csvParser(ID_file) {
	var options = {
		delimiter: '\t',
		endLine: '\n',
		columns: ['ID', 'Date', 'Water Level']
	};

	var csvStream = csv.createStream(options);
	canada_stations.forEach(function(province_name){
	var province_URL='http://dd.weather.gc.ca/hydrometric/csv/'+province_name+'/daily/'+province_name+'_daily_hydrometric.csv';
	var r = request(province_URL).pipe(csvStream);
	var obser_data = [];

	r.on('error', function(err) {
		console.error(err);
	});
	r.on('data', function(data) {
		console.time('process-time');
		totalData += data;
		console.log(totalData.length + "data has been received");
		console.log("streaming data...");
		var true_data = data.ID.split(',').splice(0, 3);
		var date1 = new Date(true_data[1]);
		date1.setUTCHours(0, 0, 0, 0);
		var date2 = new Date(new Date().setDate(new Date().getDate() - 1));
		date2.setUTCHours(0, 0, 0, 0);

		if (date1.getTime() == date2.getTime()) {
			var stationID = true_data[0];
			var time = new Date(true_data[1]);
			var waterLevel = true_data[2];
			if (ID_file.hasOwnProperty(stationID)) {
				var observation = {};
				observation.Datastream = {};
				observation.Datastream.id = ID_file[stationID];
				observation.result = waterLevel;
				observation.phenomenonTime = time;
				obser_data.push(observation);
			};

		}
	});
	r.on('end', function() {
		console.log('Saving Data...')
		var ober_dir = new Date();	
		var output_observation = ober_dir + "observations.json";
		fs.writeFile(output_observation, JSON.stringify(obser_data), function(err) {
			if (err) {
				console.log(err);
			} else {
				console.log("JSON saved to " + output_observation);
				
			}
		});       
		for (var i = obser_data.length - 1; i >= 0; i--) {
			request.post({
				uri: obserURL,
				headers: headers,
				body: JSON.stringify(obser_data[i])
			}, function(e, r, body) {
				if (e) {
					console.log(e);
				};
				console.log(body)
			});
		};
     console.timeEnd("process-time");

	})
	});
    console.log("process finished,waiting for next time running...");
}